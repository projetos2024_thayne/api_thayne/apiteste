const connect = require("../db/connect");

async function cleanUpSchedules(){
    const currentDate = new Date();
    
    //Definir a data para sete dias atrás
    currentDate.setDate(currentDate.getDate() - 7) 

    //Formata a data para YYYY-MM-DD
    const formattedDate = currentDate.toISOString().split('T')[0]; 

    const query = `DELETE FROM schedule WHERE dateEnd < ?`
    const values = [formattedDate];

    return new Promise((resolve, reject)=>{
        connect.query(query, values, function(err,result){
            if(err){
                console.error("Erro Mysql", err);
                return reject (new Error("Erro ao limpar agendamento"));
            }
            console.log("Agendamento antigos apagados");
            resolve("Agendamentos antigos limpos com sucesso");
        });
    });
}

module.exports = cleanUpSchedules;